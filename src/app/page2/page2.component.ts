import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

const { SisenseFrame, enums } = window['sisense.embed' as any] as any;

class SisenseFrameTag {};

class MyObject {};

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.css']
})
export class Page2Component implements AfterViewInit {
  @ViewChild('sisenseIframe', { static: false })
  sisenseIFrameRef?: ElementRef<HTMLElement>;

  private sisenseFrame: any;
  private myObject?: MyObject = new MyObject();
  private eventListeners: any[] = [];

  onDashboardLoaded = () => console.log('dashboard loaded');

  ngAfterViewInit(): void {
    var addEventListener = window.addEventListener;
    window.addEventListener = (...args: any[]) => {
      const [type, listener] = args;
      this.eventListeners.push({type, listener});
      addEventListener.apply(window, args as any);
    }


    this.sisenseFrame = new SisenseFrame({
      url: 'https://sisense.fire-dev.dev.eso.local/',
      element: this.sisenseIFrameRef?.nativeElement,
      dashboard: '610450a9ea6c5400366a876a',
      settings: {
        showToolbar: true,
        showLeftPane: false,
        showRightPane: false
      }
    });

    this.sisenseFrame.sisenseFrameTag = new SisenseFrameTag();

    this.sisenseFrame.dashboard.on(enums.DashboardEventType.LOADED, this.onDashboardLoaded);

    window.addEventListener = addEventListener;
  }

  ngOnDestroy(): void {
    this.myObject = undefined;
    this.sisenseFrame.dashboard.off(enums.DashboardEventType.LOADED, this.onDashboardLoaded);
    this.sisenseFrame = null;
    for(const listener of this.eventListeners) {
      window.removeEventListener(listener.type, listener.listener);
    }
  }
}