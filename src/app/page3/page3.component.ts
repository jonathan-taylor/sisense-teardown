import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

const { SisenseFrame, enums } = window['sisense.embed' as any] as any;

class SisenseFrameTag {};

@Component({
  selector: 'app-page3',
  templateUrl: './page3.component.html',
  styleUrls: ['./page3.component.css']
})
export class Page3Component implements AfterViewInit {
  @ViewChild('sisenseIframe', { static: false })
  sisenseIFrameRef?: ElementRef<HTMLElement>;

  private sisenseFrame: any;

  onDashboardLoaded = () => console.log('dashboard loaded');

  ngAfterViewInit(): void {
    this.sisenseFrame = new SisenseFrame({
      url: 'https://sisense.fire-dev.dev.eso.local/',
      element: this.sisenseIFrameRef?.nativeElement,
      dashboard: '610450a9ea6c5400366a876a',
      settings: {
        showToolbar: true,
        showLeftPane: false,
        showRightPane: false
      }
    });

    this.sisenseFrame.sisenseFrameTag = new SisenseFrameTag();

    this.sisenseFrame.dashboard.on(enums.DashboardEventType.LOADED, this.onDashboardLoaded);

  }

  ngOnDestroy(): void {
    this.sisenseFrame.dashboard.off(enums.DashboardEventType.LOADED, this.onDashboardLoaded);
    this.sisenseFrame = null;
  }
}